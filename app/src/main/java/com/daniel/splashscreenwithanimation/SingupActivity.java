package com.daniel.splashscreenwithanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class SingupActivity extends AppCompatActivity {
    DatePickerDialog picker;
    private EditText date;
    private Spinner spinnerClass;
    private ArrayList<Bike> mBike;
    private BikeAdapter mBikeAdapter;
    private Switch switch1;
    private TextView textAlready;
    private LinearLayout linearBig;
    private ObjectAnimator objectAnimator1;
    private ObjectAnimator objectAnimator2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        //Hook
        date= (EditText) findViewById(R.id.date);
        spinnerClass=findViewById(R.id.spinnerClass);
        switch1 = findViewById(R.id.switch1);
        textAlready = findViewById(R.id.textAlready);
        linearBig = (LinearLayout) findViewById(R.id.linearBig);
        objectAnimator1 = ObjectAnimator.ofFloat(linearBig, "scaleX", 0f, 1f);
        objectAnimator1.setDuration(1000);
        objectAnimator2 = ObjectAnimator.ofFloat(linearBig, "scaleY", 0f, 1f);
        objectAnimator2.setDuration(1000);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether( objectAnimator1,objectAnimator2);
        animatorSet2.start();

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Select actual date
                Calendar calendar= Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

                //DatePickerDialog
                //DatePickerDialog.OnDateSetListener()

                picker = new DatePickerDialog(SingupActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        //mostrar la data seleccionada
                        month++;
                        date.setText(dayOfMonth + "/" + month + "/" + year );
                    }
                }, year, month, day);
                picker.show();
            }
        });

        mBike = new ArrayList<>();
        mBike.add(new Bike("Mountain Bike", R.drawable.bike01));
        mBike.add(new Bike("Mountain Bike", R.drawable.bike02));
        mBike.add(new Bike("Mountain Bike", R.drawable.bike03));
        mBike.add(new Bike("Mountain Bike", R.drawable.bike04));
        mBike.add(new Bike("Mountain Bike", R.drawable.bike06));
        mBike.add(new Bike("Mountain Bike", R.drawable.bike08));


        mBikeAdapter = new BikeAdapter(this, mBike);
        spinnerClass.setAdapter(mBikeAdapter);

        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Bike selectedItem = (Bike) parent.getItemAtPosition(position);
                String selectedBikeName = selectedItem.getBikeName();
                Toast.makeText(SingupActivity.this, selectedBikeName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(SingupActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });
            switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (switch1.isChecked()){
                Toast.makeText(SingupActivity.this, "Switch activated", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(SingupActivity.this, "Nothing desactivated", Toast.LENGTH_SHORT).show();
            }
        }
    });
        textAlready.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SingupActivity.this, DashboardActivity.class );
                startActivity(intent);
            }
        });

    }



}