package com.daniel.splashscreenwithanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.LimitExceededException;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {
private LinearLayout linearBig;
private TextView textSing;
private TextView textForgot;
private ObjectAnimator objectAnimator1;
private ObjectAnimator objectAnimator2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        linearBig = (LinearLayout) findViewById(R.id.linearBig);
        objectAnimator1 = ObjectAnimator.ofFloat(linearBig, "scaleX", 0f, 1f);
        objectAnimator1.setDuration(1000);
        objectAnimator2 = ObjectAnimator.ofFloat(linearBig, "scaleY", 0f, 1f);
        objectAnimator2.setDuration(1000);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether( objectAnimator1,objectAnimator2);
        animatorSet2.start();

        textSing = (TextView) findViewById(R.id.textSing);
        textForgot = (TextView) findViewById(R.id.textForgot);


        textSing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, SingupActivity.class );
                startActivity(intent);
            }
        });
        textForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ForgotpasswordActivity.class );
                startActivity(intent);
            }
        });

    }
}