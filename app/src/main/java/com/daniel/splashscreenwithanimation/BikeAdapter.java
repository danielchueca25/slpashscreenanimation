package com.daniel.splashscreenwithanimation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BikeAdapter extends ArrayAdapter<Bike> {
    public BikeAdapter( Context context, ArrayList<Bike> bikeArrayList){
        super(context,0,bikeArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);

    }



    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @NonNull
    private View initView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView ==null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent,false);
        }
        ImageView imageView = convertView.findViewById(R.id.imageBike);
        TextView textView = convertView.findViewById(R.id.textBike);

        Bike currentItem = getItem(position);

        if (currentItem!=null){
            imageView.setImageResource(currentItem.getBikeImg());
            textView.setText(currentItem.getBikeName());
        }
        return convertView;
    }
}

