package com.daniel.splashscreenwithanimation;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static int SPLASH_SCREEN = 2000;
    private ImageView imageView;
    private TextView textView;
    private TextView textView3;
    private LinearLayout linearBig;

    private ObjectAnimator objectAnimator1;
    private ObjectAnimator objectAnimator2;
    private ObjectAnimator objectAnimator3;
    private ObjectAnimator objectAnimator6;
    private ObjectAnimator objectAnimator4;
    private ObjectAnimator objectAnimator5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hooks
        imageView= (ImageView) findViewById(R.id.imageBike);
        textView = (TextView) findViewById(R.id.textBike);
        textView3 = (TextView) findViewById(R.id.textView3);
        objectAnimator1 = ObjectAnimator.ofFloat(imageView, "y", -550f, 0f);
        objectAnimator1.setDuration(1000);





        /*objectAnimator3 = ObjectAnimator.ofFloat(textView3, "y", 2000f, 570f);
        objectAnimator2.setDuration(1000);*/

        //objectAnimator4 = ObjectAnimator.ofFloat(textView, "rotationX", 0f, 180f);
        //objectAnimator5 = ObjectAnimator.ofFloat(imageView, "rotationX", 0f, 360f);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether( objectAnimator1);
        animatorSet2.start();

        Runnable r =new Runnable() {
            @Override
            public void run() {
                //intent
                Intent intent= new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View,String>(imageView,"imageapp");
                pairs[1] = new Pair<View,String>(textView,"textapp");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,pairs);
                startActivity(intent,options.toBundle());
            }
        };

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, SPLASH_SCREEN);
    }
}